dev:
	npm run dev

build:
	npm run build

preview:
	npm run preview

purge:
	rm rf ./node_modules
