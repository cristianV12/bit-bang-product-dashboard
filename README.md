# bit-bang-product-dashboard

Solución a prueba técnica para The Bit Bang Company

El proyecto usa Vite para la empaquetación de módulos(module bundler).

**Para ejecutar el proyecto usar el comando:**

`npm run dev`

O como alternativa usar **make**:

`make dev`




**Notas:**

**Dependencias y herramientas usadas**

Además de vite, el proyecto usa styled-components para los estilos y primereact como librería de componentes

Componentes usados de PrimeReact: 
  
- Button
- Dialog
- Paginator

**Sobre persistencia**
Se implementa una solución para la persistencia con localstorage
los datos mockeados se encuentran en la carpeta data,
para usar estos datos descomentar la linea que setea los datos en App.jsx
