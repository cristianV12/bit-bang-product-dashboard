// A product form component for creating new products,
// Using the controlled component pattern
import { FormWrapper } from "./styled/FormWrapper"

const CreateProductForm = ({onSubmit, newProduct, onChange}) => {
  return (
    <FormWrapper onSubmit={onSubmit} className='product-form'>
      <div className="product-form_field">
        <label htmlFor="name">Name:</label>
        <input
          name='name'
          id="name"
          className='product-form_input'
          placeholder="Product Name"
          value={newProduct.name}
          onChange={onChange}
          required
          type="text" />
      </div>
      <div className="product-form_field">
        <label htmlFor="price">Price:</label>
        <input
          name='price'
          id="price"
          className='product-form_input'
          value={newProduct.price}
          onChange={onChange} 
          required
          type="number"/>
      </div>
      <div className="product-form_field">
        <label htmlFor="image">Image:</label>
        <input
          name='image'
          id="image"
          placeholder="(URL): https://example.com/image.png"
          className='product-form_input'
          value={newProduct.image}
          onChange={onChange} 
          required
          type="text"/>
      </div>
      <div className="product-form_field">
        <label htmlFor="description">Description:</label>
        <input
          name='description'
          id="description"
          placeholder="describe for your product"
          className='product-form_input'
          value={newProduct.description}
          onChange={onChange} 
          required
          type="text"/>
      </div>
      <div className="product-form_field">
        <label htmlFor="quantity">Quantity:</label>
        <input
          name='quantity'
          id="quantity"
          className='product-form_input'
          value={newProduct.quantity}
          onChange={onChange}
          required 
          type="number"/>
      </div>
      <input 
        type="submit" 
        className='product-form_submit' 
        value='Add Product'/>
    </FormWrapper>
  )
}

export {CreateProductForm}