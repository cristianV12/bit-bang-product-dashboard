import styled from "styled-components"
import React, {useState} from "react"
import { Paginator } from 'primereact/paginator'

import { ProductCard } from "./styled/ProductCard"
import { ProductDetail } from "./ProductDetail"


const ProductGrid = styled.div `
  display: grid;
  place-items: center;
  gap: 0.5rem;
  width: 35vw;
  min-width: 350px;
  padding: 1rem;
`

// Component to show and render the products list
const ShowProducts = ({products, setProducts}) => { 

  // paginator settings
  const productLimit = 3
  const [basicFirst, setBasicFirst] = useState(0)


  const [currentProduct, setCurrentProduct] = useState({})
  const [viewProductDetail, setViewProductDetail] = useState(false)


  const handleDetail = (event) => {
    event.stopPropagation()
    const productIndex = event.currentTarget.id - 1
    const currentProduct = products[productIndex]
    
    setCurrentProduct(currentProduct)
    setViewProductDetail(true)
  }

  const reserveProductHandler = (event) => {
    event.stopPropagation()
    const productCopy = [...products]
    
    if(!currentProduct.quantity) return
    
    productCopy[currentProduct.id - 1].quantity -= 1
    setProducts(productCopy)
    localStorage.setItem('products', JSON.stringify([...productCopy]))

    setViewProductDetail(false)

  }

  // paginator filter
  const productList = products.slice(basicFirst, basicFirst + productLimit)
  
  const onPageChange = (event) => {
    setBasicFirst(event.first)
  }
  
  // creating the product cards
  // based on product info pushed to products list
  const renderProducts =  productList.map(product => (
    <ProductCard 
      key={product.id} 
      id={product.id}
      onClick={handleDetail}
      className="product-card">
      <div 
        className="product-card_image"
        style={{backgroundImage: `url(${product.image})`}}
      ></div>
      <div className="product-card_info">
        <h2>{product.name}</h2>
        <p className="product-description">{product.description}</p>
        <p><strong>Price:</strong> {product.price}$</p>
        <p><strong>Quantity:</strong> {product.quantity}</p>
      </div>
    </ProductCard>
  ))

  if (!products.length) {
    return (
      <div className="show-no-products">
        <h1>No products yet</h1>
        <p>please add a new product</p>
      </div>
    )
  } else {
    return (
      <div className="show-product">
        <ProductGrid className="product-grid">
          {renderProducts}
        </ProductGrid>
        <Paginator
          first={basicFirst}
          rows={productLimit}
          totalRecords={products.length}
          onPageChange={onPageChange}
        ></Paginator>
        <ProductDetail 
          visible={viewProductDetail}
          onHide={() => setViewProductDetail(false)}
          currentProduct={currentProduct}
          onClick={reserveProductHandler}/>
      </div>
    )
  }

}

export {ShowProducts}