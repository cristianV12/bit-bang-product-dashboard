import { Dialog } from "primereact/dialog"
import { Button } from 'primereact/button'

import styled from "styled-components"

const ProductDetailWrapper = styled.div`

    display: grid;
    place-items: center;
    grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));
    gap: 1rem;
    .product-detail {
      &_image {
        width: 100%;
        height: 200px;
        background-repeat: no-repeat;
        background-position: center;
        background-size: contain;
        border: 1px solid lightgray;
        border-radius: 6px;
      }

      &_info {
        display: grid;
        padding: 1rem;
      }
    }
`


const ProductDetail = ({visible, onHide, currentProduct, onClick}) => {
  return (<Dialog 
    header="Product Detail"
    className="product-detail"
    visible={visible}
    style={{width: '50vw', minWidth:'330px'}}
    onHide={onHide}
  >
    <ProductDetailWrapper className="product-detail_card">
      <div 
        className="product-detail_image"
        style={{backgroundImage: `url(${currentProduct.image})`}}
      ></div>
      <div className="product-detail_info">
        <p><strong>Name:</strong> {currentProduct.name} </p>
        <p><strong>Description</strong> {currentProduct.description}</p>
        <p><strong>Price:</strong> {currentProduct.price}</p>
        <p><strong>Quantity:</strong> {currentProduct.quantity}</p>
        <Button onClick={onClick} label="Reserve"/>
      </div>
    </ProductDetailWrapper>
  </Dialog>)
}

export {ProductDetail}