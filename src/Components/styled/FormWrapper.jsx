import styled from "styled-components"

const FormWrapper = styled.form`
  display: grid;
  gap: 1rem;
  

  .product-form {
    &_field {
      display: inherit;
      width: 100%;
      gap: 0.3rem;
    }

    &_input {
      padding: 0.5rem;
      border-radius: 4px;
      border: 1px solid black;
      font-size: 1.05rem;
    }

    &_submit {
      padding: 1rem;
      border-radius: 4px;
      border: 1px solid lightgray;
      cursor: pointer;
      font-size: 1rem;
    }
  }
`

export {FormWrapper}