import styled from "styled-components";

const ProductCard = styled.div`
  display: grid;
  gap: 0.5rem;
  grid-template-columns: 0.2fr 1fr;
  border: 2px solid lightgrey;
  padding: 1rem;
  border-radius: 10px;
  align-content: center;
  align-items: center;
  width: 100%;
  height: fit-content;
  cursor: pointer;
  transition: all 0.1s ease-in-out;

  &:hover {
    transform: scale(1.02);
  }

  .product-card {
    &_image {
      width: 100px;
      height: 100px;
      background-repeat: no-repeat;
      background-size: contain;
      background-position: center;
    }

    &_info {
      display: grid;
      gap:0.3rem;

      .product-description {
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
      }

      & > * {
        margin: 0;
      }
    }

  }

`
export { ProductCard }