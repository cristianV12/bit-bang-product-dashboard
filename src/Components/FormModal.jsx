import { CreateProductForm } from "./CreateProductForm"
import { Dialog } from "primereact/dialog"
import { Button } from "primereact/button"
import styled from "styled-components"


const FormModal = ({onSubmit, newProduct, onChange, visible, onHide, onClick}) => {
  return (
      <div className="create-product_modal">
        <Button label="New Product" icon="pi pi-plus" onClick={onClick} />
        <Dialog 
          header="Create New Product" 
          className="create-product_dialog"
          visible={visible} 
          style={{ width: '50vw', minWidth: '330px' }} 
          onHide={onHide}>
          <CreateProductForm 
            onSubmit={onSubmit} 
            newProduct={newProduct} 
            onChange={onChange} />
        </Dialog>
      </div>
  )
}

export { FormModal }