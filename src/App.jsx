import { useState } from 'react'
import './App.css'
// Import main components
import { FormModal } from './Components/FormModal'
import {ShowProducts} from './Components/ShowProducts'

import logo from './assets/logo.png'
import mockProductData from './data/burned-products.json'
// Import PrimeReact library
import 'primereact/resources/themes/lara-light-indigo/theme.css'
import 'primereact/resources/primereact.min.css'
import 'primeicons/primeicons.css'


const App = () => {
  
  /* 
    uncomment the next line 
    only to obtain mock data
    to test the prototype 
  */

  // localStorage.setItem('products', JSON.stringify(mockProductData))
  
  
  //Set Dialog Modal
  const [displayBasic, setDisplayBasic] = useState(false);
  
  // configuring the states for products
  // I need one for the product list
  
  const [products, setProducts] = useState(() => {
    const saved = localStorage.getItem('products')
    const initialValue = JSON.parse(saved);
    return initialValue ? initialValue : []
  })


  // and one for setting a new product with the product model
  
  const [newProduct, setNewProduct] = useState({
    name: '',
    price: 0,
    image: '',
    description: '',
    quantity: 0
  })
    
  // onSubmit handler to push the products to the list
  const onSubmitHandler = (event) => {
    event.preventDefault()

    // create a product object right from the inputs 
    // captured with onChange attribute
    const productObject = {
      name: newProduct.name,
      price: newProduct.price,
      image: newProduct.image,
      description: newProduct.description,
      quantity: newProduct.quantity,
      id: products.length + 1
    }

    //pushing the product object to products list
    setProducts(products.concat(productObject))
    localStorage.setItem('products', JSON.stringify([...products, productObject]))

    // Clear the inputs
    setNewProduct({
      name: '',
      price: '',
      image: '',
      description: '',
      quantity: '',
    })

    setDisplayBasic(false)
  }

  // Setting a new product
  const handleProduct = (event) => {
    // capturing the inputs value
    const value =  event.target.value

    // Using name attributes to set the product object
    setNewProduct({
      ...newProduct,
      [event.target.name] : value
    })
  }

  return (
    <div className="App">
      <header className="App-header">
        <img className="App-logo" src={logo} alt="App Logo" />
        <span>BitBang Product Dashboard</span>
      </header>

      <ShowProducts products={products} setProducts={setProducts}/>
      <FormModal 
        onSubmit={onSubmitHandler}
        newProduct={newProduct}
        onChange={handleProduct}
        visible={displayBasic}
        onClick={() => setDisplayBasic(true)}
        onHide={() => setDisplayBasic(false)}
        />
    </div>
  )
}

export default App
